# Pleroma: A lightweight social networking server
# Copyright © 2021 Emily Rowlands
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Pleroma.Web.ActivityPub.MRF.EitherSubchainPolicy do
  alias Pleroma.Config
  alias Pleroma.Web.ActivityPub.MRF

  require Logger

  @behaviour Pleroma.Web.ActivityPub.MRF

  def is_ok({:ok, _}), do: true
  def is_ok(_), do: false

  @impl true
  def filter(message) do
    subchains = Config.get([:mrf_either_subchain, :subchains])

    with [{{:ok, result}, subchain} | _] <- subchains
      |> Stream.map(fn subchain -> {MRF.filter(subchain, message), subchain} end)
      |> Stream.drop_while(fn {result, _} -> !is_ok(result) end)
      |> Stream.take(1)
      |> Enum.to_list() do
      Logger.debug("[EitherSubchainPolicy] Accepting from subchain #{inspect(subchain)}")
      {:ok, result}
    else 
      [] -> {:reject, "[EitherSubchainPolicy] No accepts in any subchain"}
      [value] -> {:reject, "[EitherSubchainPolicy] UHHH? Something real weird happened: #{inspect(value)}"}
    end
  end

  @impl true
  def describe do
    {:ok, %{mrf_either_subchain:
      Config.get([:mrf_either_subchain, :subchains])
        |> Enum.map(fn chain -> chain |> Pleroma.Web.ActivityPub.MRF.describe() end)
    }}
  end
end
