# Pleroma: A lightweight social networking server
# Copyright © 2021 Emily Rowlands
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Pleroma.Web.ActivityPub.MRF.DebugPolicy do
  @moduledoc "Does nothing (lets the messages go through unmodified) and logs the message"

  require Logger

  @behaviour Pleroma.Web.ActivityPub.MRF

  @impl true
  def filter(object) do
    Logger.warn("[DebugPolicy] #{inspect(object)}")
    {:ok, object}
  end

  @impl true
  def describe, do: {:ok, %{}}
end
