# Pleroma: A lightweight social networking server
# Copyright © 2021 Emily Rowlands
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Pleroma.Web.ActivityPub.MRF.StripAllExcept do
  @moduledoc "Strips all recipients except the specified"

  alias Pleroma.Config
  alias Pleroma.User

  require Pleroma.Constants
  require Logger

  @behaviour Pleroma.Web.ActivityPub.MRF

  def is_local?(recipient) do
    String.starts_with?(recipient, Pleroma.Web.Endpoint.url())
  end

  def is_public?(recipient) do
    recipient == Pleroma.Constants.as_public()
  end

  def strip_disallowed(to, allowed_recipients) do
    Enum.filter(to, fn recipient -> recipient in allowed_recipients or not (is_local?(recipient) or is_public?(recipient)) end)
  end

  def filter_this_thing(thing) do
    policy = Config.get(:mrf_stripallexcept)
    allowed_recipients = Keyword.get(policy, :allowed_recipients)

    to = strip_disallowed(thing["to"], allowed_recipients)
    cc = strip_disallowed(thing["cc"], allowed_recipients)

    thing = thing
      |> Map.put("to", to)
      |> Map.put("cc", cc)
    thing
  end

  def do_strip(%{"object" => object} = message) do
    message = filter_this_thing(message)
      |> Map.put("object", filter_this_thing(object))

    if message["to"] == [] do
        {:reject, "[StripAllExcept] Rejected\nto: #{inspect(message["to"])}\ncc: #{inspect(message["cc"])}"}
    else
        {:ok, message}
    end
  end

  @impl true
  def filter(%{"type" => "Create", "object" => object} = message) do
    user = User.get_cached_by_ap_id(object["actor"])

    visibility =
      cond do
        Pleroma.Constants.as_public() in object["to"] -> "public"
        Pleroma.Constants.as_public() in object["cc"] -> "unlisted"
        user.follower_address in object["to"] -> "followers"
        true -> "direct"
      end

    cond do
      visibility in ["public", "unlisted", "followers"] ->
        {:reject, "[StripAllExcept] Post too visible: #{visibility}, from: #{object["actor"]}"}
      true ->
        do_strip(message)
    end

  end

  @impl true
  def filter(%{"type" => type, "to"=> to, "actor"=>actor} = message) do
    disallowed_types = ["Follow"]
    if type in disallowed_types do
      Logger.warn("[StripAllExcept] Disallowed type: #{type} To: #{to} From: #{actor}")
      {:reject, "[StripAllExcept] Disallowed type: #{type}"}
    else
      Logger.warn("[StripAllExcept] Type: #{type}\n#{inspect(message)}")
	  {:ok, message}
	end
  end

  @impl true
  def filter(%{"type" => "Application"} = message), do: {:ok, message}

  @impl true
  def filter(%{"type" => "Person", "suspended" => true} = message), do: {:ok, message}

  @impl true
  def filter(%{"type" => "Person", "id"=> id} = _message), do: {:reject, "[StripAllExcept] Rejected user: #{id}"}

  @impl true
  def filter(%{"type" => "Service", "id"=> id} = _message), do: {:reject, "[StripAllExcept] Rejected service: #{id}"}

  @impl true
  def filter(%{"type" => type} = message) do
    Logger.warn("[StripAllExcept] Accepted unknown type: #{type}, #{inspect(message)}")
    {:ok, message}
  end

  @impl true
  def describe,
    do: {:ok, %{mrf_stripallexcept: "Test"}}
end
